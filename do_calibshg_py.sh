#!/bin/sh

#Run Python program to calibrate wavelength axes of SHG cubes 
#
# Usage: do_calibshg_py.sh >> do_calibshg_py.log 2>&1
#
#
#add path to python3  (see  /etc/profile.d/softs-env.sh )
export PATH=/opt/softs-centos7/BIN:/opt/softs-centos7/anaconda2/bin/:$PATH
export PYTHONPATH=/home/corbard/PYTHON
DATE=`date +%Y%m%d%H%M`
echo ""
echo "******Start time : " `date "+%Y-%m-%d %H:%M:%S"` "******************"
echo Report of SHG cube calibration.
echo Script is /home/corbard/SCRIPTS/CRONJOBS/SHG/do_calibshg_py.sh

source activate py3
export MPLBACKEND="agg" #allow to create figures without any X-server running
echo "Execute auto_update_profilefit.py ha cah cak"
/home/corbard/SCRIPTS/auto_update_profilefit.py ha cah cak &>/mingus-DEPOT/CRONLOG/SHG/do_calibshg_py_$DATE.log
#If at least one file has been processed then e-mail a report
if [ $? == 2 ] 
then
  mail -s 'do_calibshg_py: log' Thierry.Corbard@oca.eu < /mingus-DEPOT/CRONLOG/SHG/do_calibshg_py_$DATE.log
fi

echo "log file: /mingus-DEPOT/CRONLOG/SHG/do_calibshg_py_$DATE.log"
echo "******Stop time : " `date "+%Y-%m-%d %H:%M:%S"` "******************"
echo ""

