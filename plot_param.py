#!/usr/bin/env python3

import numpy as np
import plotly.graph_objects as go
import pandas as pd
from datetime import datetime
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import os,sys
from shgcalib.tc_shgcalib import default_key 

def plotly_param(cak_csv,cah_csv,ha_csv,
                 figpath_root='./',
                 fig_name='spectro_obspm_fullprofile_CAL.html',
                 WSSR_limit={'cak':np.inf,'cah':np.inf,'ha':np.inf},
                 html=False,show=True):

    cak = pd.read_csv(cak_csv,index_col=0,parse_dates=True,sep='\t')
    cah = pd.read_csv(cah_csv,index_col=0,parse_dates=True,sep='\t')
    ha  = pd.read_csv(ha_csv, index_col=0,parse_dates=True,sep='\t')

    cak=cak[abs(cak['CAL_WSSR']) < WSSR_limit['cak'] ] 
    cah=cah[abs(cah['CAL_WSSR']) < WSSR_limit['cah'] ] 
    ha=  ha[abs( ha['CAL_WSSR']) < WSSR_limit['ha'] ]

    if html:
        html_str="""
        <html>
        <head><meta charset="utf-8" /></head>
        <body>
        <div>
        <script type="text/javascript">window.PlotlyConfig = {MathJaxConfig: 'local'};</script>
        <script src="https://cdn.plot.ly/plotly-latest.min.js"></script>
        """

    for var,cmt,_ in (default_key[0],)+default_key[3:]:
        fig = go.Figure(layout_title_text=f'{var}: {cmt}')
        fig.add_trace(go.Scatter(x=cak.index,y=cak[var],mode='markers',name='CaIIK'))
        fig.add_trace(go.Scatter(x=cah.index,y=cah[var],mode='markers',name='CaIIH'))
        fig.add_trace(go.Scatter(x=ha.index,y=ha[var],mode='markers',name='Ha'))
        fig.update_layout(xaxis_range=[min([cak.index.min(),cah.index.min(),ha.index.min()]),
                                       max([cak.index.max(),cah.index.max(),ha.index.max()])])
        if html:
            fig.update_layout(height=400)
        fig.update_xaxes(
            rangeslider_visible=False,
            rangeselector=dict(
                buttons=list([
                    dict(count=1,label="1d",step="day",stepmode="backward"),
                    dict(count=7,label="1w",step="day",stepmode="backward"),
                    dict(count=1, label="1m", step="month", stepmode="backward"),
                    dict(count=6, label="6m", step="month", stepmode="backward"),
                    dict(count=1, label="YTD", step="year", stepmode="todate"),
                    dict(count=1, label="1y", step="year", stepmode="backward")#,
                    #dict(step="all")
                ])
            )
        )
        if html:
            html_str+=fig.to_html(include_plotlyjs=False,full_html=False)
        if show:
            fig.show(config=dict({'scrollZoom': False}))

    
    if html:
        html_str+="""
        </div>
        </body>
        </html>
        """
        with open(os.path.join(figpath_root,fig_name), 'wt') as html_file:
            html_file.write(html_str)
        

def plot_param(csvfile,fig=False,show=True,figpath_root='./',WSSR_limit=np.inf):

    years = mdates.YearLocator()   # every year
    months = mdates.MonthLocator()  # every month
    yearsFmt = mdates.DateFormatter('%Y')
    mthFmt = mdates.DateFormatter('')
    
    data=pd.read_csv(csvfile,index_col=0,parse_dates=True,sep='\t')

    good=abs(data['CAL_WSSR'])<=WSSR_limit
    
    list_of_datetimes=[datetime.fromisoformat(str(dateobs)) 
                       for dateobs in data.index] 
    dates = matplotlib.dates.date2num(list_of_datetimes)

    for var in data.columns[3:]: 
        fig, ax = plt.subplots()
        ax.plot(dates[good], data[var][good],'+')
        ax.xaxis.set_major_locator(years)
        ax.xaxis.set_major_formatter(yearsFmt)
        ax.xaxis.set_minor_locator(months)
        ax.xaxis.set_minor_formatter(mthFmt)
        ax.grid(True)
        ax.set_title(var)
        #fig.autofmt_xdate()
        if(fig):
            fig.savefig(os.path.join(figpath_root,var+'.png'))
    if(show):
        plt.show()
if __name__=="__main__":
    if len(sys.argv)>1:
        import argparse
        parser = argparse.ArgumentParser()
        parser.add_argument("file", help="Data file (ascii)")
        args = parser.parse_args()
        plot_param(args.file,show=True,fig=False)
    else:
        #print('Please enter the ascii data file name')
        plotly_param(cak_csv='ftp://solar-ftp.oca.eu/SHG/CAIIK/spectro_obspm_fullprofile_Cak_3D.csv',
                     cah_csv='ftp://solar-ftp.oca.eu/SHG/CAIIH/spectro_obspm_fullprofile_Cah_3D.csv',
                     ha_csv ='ftp://solar-ftp.oca.eu/SHG/HA/spectro_obspm_fullprofile_ha_3D.csv',
                     html=False,show=True)
                     #figpath_root='/mingus-var-ftp/SHG/',
                     #WSSR_limit={'ha':100_000,'cak':10_000,'cah':10_000})
