**Wavelength axis calibration of Spectro-Heliographe datacubes**

[[_TOC_]]

## Method

Let us call $`C[i,j,k]`$ the datacubes provided by the Spectroheliograph FITS files with $`ì, j, k`$ varying from 1 to respectively NAXIS1, NAXIS2 and NAXIS3 (pixel numbers). 
They need to be calibrated along the  wavelength axis (k) in order to compute header CRPIX3 and CRVAL3 keywords (line center in pixel unit and in Angstrom,
respectively (see [Greisen and Calabretta (2002)](https://doi.org/10.1051/0004-6361:20021326). 
They are roughly  known in advance but can be precisely determined. We first locate the minimum of the integrated intensities of the cube images:
```math
S_\mathrm{obs}[k]=\frac{\sum_{i,j}C[i,j,k]}{\mathrm{NAXIS1}\times\mathrm{NAXIS2}} \ \ \ \ \ \ \  k=1...\mathrm{NAXIS3} \ \ \ \ \ \ \ [1]
```
```math
k_\mathrm{min}=\mathrm{argmin}\left(S_\mathrm{obs}[k]\right)  \ \ \ \ \ \ \ [2]
```
This minimum of $`S_\mathrm{obs}`$ at index $`k_\mathrm{min}`$ may not be strictly at  the  line core location $`\lambda_\mathrm{c}`$ (keyword WAVELNTH).
The idea is then to fit the observed line profile with a reference one  $`S_{ref}(\lambda)`$ in order to identify precisely 
the location of the minimum and to assign to it the value of  $`\lambda_c`$. 
The reference spectrum is taken from the visible atlas by [Delbouille et al. (1973)](http://bass2000.obspm.fr/Atlas_visible.pdf) available online on the  [BASS2000 website](http://bass2000.obspm.fr/solar_spect.php) 
with a resolution of $`\bar{\delta\lambda}`$=0.002 Å. 

The bandwidth for each image of the cube is assumed unknown and therefore the reference spectrum is smoothed over a number of 
points $`n`$  that we also have to determine. We define the smoothed reference spectra $`\bar{S}_\mathrm{ref}(\lambda)`$ to which we want to compare the observed one by: 
```math
\bar{S}_\mathrm{ref}(\lambda)=\frac{1}{\delta\lambda}\int_{\lambda-\delta\lambda/2}^{\lambda+\delta\lambda/2} S_\mathrm{ref}(\bar{\lambda})\  d\bar{\lambda}\ \ \ \ \mathrm{with}\ \ \ \delta\lambda=n\ \bar{\delta\lambda} \ \ \ \ \ \ \ [3]
```
The minimum value of this smoothed spectra is located at $`\lambda_{s}(n)`$  which is a function of  $`n`$ and is slightly different from $`\lambda_\mathrm{c}`$.
The wavelengths corresponding to the different images of the cube are obtained by : 
```math
\lambda_k=\lambda_s(n)+(k-k_{min})*\Delta\lambda
```
where $`\Delta\lambda`$  is given by the FITS keyword CDELT3 [4]:

| line                  | Before 2017/10/15| After 2017/10/15   |
| --------------------- |:----------------:| :-----------------:|
| CaII H                |   0.0992 Å       |    0.0929 Å        | 
| CaII K                |   0.0995 Å       |    0.0932 Å        |
| $`\mathrm{H}_\alpha`$ |   0.1660 Å       |    0.1553 Å        |

 and $`k_\mathrm{min}`$ is an unknown fractional index. We assign $`\lambda_\mathrm{c}`$ to the  keyword CRVAL3  and  compute CRPIX3 through:
```math
\mathrm{CRPIX3}=k_{min}+\frac{\lambda_\mathrm{c}-\lambda_s(n)}{\Delta\lambda}
```
A linear scaling between the smoothed reference spectra and the observed may not be always appropriate for wavelengths
far from the line core. We allow a wavelength dependence of the linear term and write:
```math
\hat{S}_\mathrm{obs}[k]=a+b(\lambda_k)\ \bar{S}_\mathrm{ref}(\lambda_k) \ \ \ \ \ \ \  k=1...\mathrm{NAXIS3} \ \ \ \ \ \ \ [5]
```
where:
```math
b(\lambda_k)=b+c (\lambda_k-\lambda_\mathrm{c})+d (\lambda_k-\lambda_\mathrm{c})^2
```
The six free parameters of the model are therefore:  $`a, b, c, d, n`$ and $`k_\mathrm{min}`$ . They could be obtained by minimizing the sum of squared residuals:
```math
\mathrm{SSR}=\sum_k {\left({S}_\mathrm{obs}[k]-\hat{S}_\mathrm{obs}[k]\right)^2} \ \ \ \ \ \ \ [6]
```
This quantity however will depend on the scaling parameters and will not permit an easy comparison for the goodness of the fit between different data cubes (quality index).
We therefore prefer instead to build, from the observed profile, a profile that can always be compared to the reference one. From Eq. (2), we  define:
```math
\hat{S}_\mathrm{ref}[k]=\frac{{S}_\mathrm{obs}[k]-a}{b(\lambda_k)}  
```
Furthermore, because we are mainly interested in  the precise minimum location, we use a  weighting function  chosen in order to give maximum weight to the points close 
to the line core: 
```math
w[k]=\frac{1}{(\lambda_k-\lambda_s(n))^4+1}
```
The six model parameters are thus obtained by minimizing a weighted sum of the squared residuals (WSSR):
```math
\mathrm{WSSR}\left(a,b,c,d,n,k_\mathrm{min}\right)=\frac{\sum_k w[k]{\left(\hat{S}_\mathrm{ref}[k]-\bar{S}_\mathrm{ref}(\lambda_k)\right)^2}}{\sum_k{w[k]}}
```
We perform a multidimensional minimization using the Nelder-Mead  simplex algorithm [(Gao and Han, 2012)](https://doi.org/10.1007/s10589-010-9329-3).
For the initial parameters we take $`n=2\Delta\lambda/\bar{\delta\lambda}`$, $`k_\mathrm{min}`$ is obtained from Eq. (2) and, given these two values,
$`a, b, c, d`$ are the least-squares solution to the linear matrix equation (5) which minimize the sum of squared residuals SSR of Eq. (6).
Because the observed minimum is not always the one close to the line core, we identify the two deepest local minima  of the observed profile and choose the one 
that leads to the minimum SSR value.

## Examples

For CaII-K spectral line, Fig. 1 of the examples jupyter notebook shows  the observed integrated intensities $`S_\mathrm{obs}`$ (Eq. (1)) compared 
to the scaled smoothed reference spectrum $`\hat{S}_\mathrm{obs}`$ (Eq. (5)) using the  initial parameters (middle panel) and after optimization (bottom panel).
Exemples of  optimized parameters for CaII-H and $`\mathrm{H}_\alpha`$ lines are shown on Fig. 2.

[See the examples jupyter notebook](shgcalib_examples.ipynb)

## Tutorial

[See the tutorial jupyter notebook](tc_shgcalib.ipynb)

## Results

Results for CaII H, K and $`\mathrm{H}_\alpha`$ lines are obtained hourly by running [auto_update_profilefit.py](auto_update_profilefit.py): `auto_update_profilefit.py ha cah cak`
through the cronjob [do_calibshg_py.sh](do_calibshg_py.sh). Plots and fit parameters for all datacubes are available [here](https://lagrange.oca.eu/fr/lag-physol-service?option=com_content&view=article&id=2723:shg-calib&catid=265) 

## Reference

Malherbe, J.-M., Corbard, T., Dalmasse. K., Optical instrumentation for chromospheric monitoring during solar cycle 25 at Paris and Côte d'Azur observatories. Journal of Space Weather and Space Climate 10 (2020) 31, EDP sciences, Topical Issue - Space Weather Instrumentation, (2020) [doi:10.1051/swsc/2020032](https://doi.org/10.1051/swsc/2020032)



