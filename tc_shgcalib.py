#!/usr/bin/env python3
'''
    Wavelength axis calibration for Meudon Spectroheliograph 
    datacubes (*_fullprofile_*.fits).

   REFERENCE: 
    ftp://solar-ftp.oca.eu/pub/DEPOT/RESULTS/SHG_CAIIK/fullprofile_Cak_3D_calib_v2.pdf

   EXAMPLES:
   
    0- Run as script at unix prompt without argument => download files
       from Bass2000 ftp server and run tests for the 6 examples of the 
       reference doc.

       $ ./tc_shgcalib.py 
 
    1- Run as script at unix prompt with filename as argument 
       (tc_shgcalib.py -h for help)
 
       1.1 Display plots and shows new FITS keywords but does not 
           update FITS file:

        $ ./tc_shgcalib.py spectro_obspm_fullprofile_Cak_3D_20181210_104007.fits.gz
       1.2 Not displaying plots and updating FITS file header:

        $ tc_shgcalib.py --noplot --update spectro_obspm_fullprofile_Cak_3D_20181210_104007.fits.gz

    2- Same as 1.1 above but from python3 command line

        >>> import tc_shgcalib
        >>> file='spectro_obspm_fullprofile_Cak_3D_20181210_104007.fits.gz' 
        >>> reffile='3934.fits'

        >>> P,h=tc_shgcalib.profilefit(file,reffile,plot=True,showall=True)
        >>> print(h['DATE_OBS'])
        >>> for (key,cmt,fmt) in tc_shgcalib.default_key:
        >>>     print(key,'=',tc_shgcalib.fitsfmt(P[key],fmt),'/',cmt)    


    3-Calibration is done calling profilefit() with no figure 
      displayed or saved. header and file are not updated. 
      The figure of the fit is displayed using the output of profilefit()
      by subsequent call to make_specobs(), load_specref() and plot_fit()
      The first figure shows the scaled smoothed reference spectrum overploted
      on the data. The second plot shows the scaled data overploted on the full
      resolution reference spectrum.  

        >>> from astropy.io import fits
        >>> data = fits.getdata(file,header=True)
        >>> refdir='./' # or location of 3934.fits
    
        >>> P,h=tc_shgcalib.profilefit(data,refdir=refdir)
        >>> specobs=tc_shgcalib.make_specobs(data)
        >>> ref=tc_shgcalib.load_specref(refdir=refdir,WAVELNTH=h['WAVELNTH'])
        >>> fig=tc_shgcalib.plot_fit(specobs,ref,*P.values(),title=h['DATE_OBS'])[0]
        >>> fig.show()
        >>> fig2=tc_shgcalib.plot_fit(specobs,ref,*P.values(),title=h['DATE_OBS'],
        >>>                           yscale='ref_full')[0]
        >>> fig2.show()

    4-Update file header with 'QI' keyword instead of the default 'CAL_WSSSR'

        >>> key= [list(x) for x in tc_shgcalib.default_key]
        >>> key[7][0]='QI'
        >>> P,h=tc_shgcalib.profilefit(file,update=True,key=key)
        >>> print(repr(h))

    5-Get the wavelengths for all images of the cube from the updated header
     
        >>> lbda=tc_shgcalib.fitskey2axe(NAXIS=h['NAXIS3'],CRPIX=h['CRPIX3'],
        >>>                              CRVAL=h['CRVAL3'],CDELT=h['CDELT3'])
        >>> print(lbda)

    For More Examples, see bellow or:
        >>> help(tc_shgcalib.profilefit)

    ------------------------------------------
    Version: 1.0 Adapted from IDL (Dec 2018)
             2.0 New fitting formula: Sobs(lbda)=a+b(lbda)*Sref(lbda) 
                 b(lbda)=b+c*(lbda-lbda_c)+d*(lbda-lbda_c)^2
    Author: Thierry.Corbard@oca.eu
    ------------------------------------------
'''
import numpy as np
import math
import matplotlib.pyplot as plt
from astropy.io import fits
import os,sys,glob,errno,decimal,warnings
import datetime
from scipy.optimize import minimize
import collections

_debug=False

#Default name/comment/format of the Keywords created or updated by this module
default_key=(('CRPIX3','[pix] Estimated from wavelength calibration','{:<.3f}'),
             ('CRVAL3','[Angstrom]','{:<.3f}'),
             ('CDELT3','[Angstrom/pix]','{:<.4f}'),
             ('CAL_A' ,'Wavelength axis calibration (offset)','{:<.3f}'),
             ('CAL_B' ,'Wavelength axis calibration (linear term)','{:<.3f}'),
             ('CAL_C' ,'Wavelength axis calibration (lambda term)','{:<.4e}'),
             ('CAL_D' ,'Wavelength axis calibration (lambda^2 term)','{:<.4e}'),
             ('CAL_DL','[Angstrom] Wavelength axis effective resolution','{:<.4f}'),
             ('CAL_WSSR','Wavelength axis calibration quality index','{:<d}'))

#Extract format strigns
_fmt=tuple([t[2] for t in default_key])

#----------------
def fitsfmt(v,fmt):
#----------------
    '''format float and integers for FITS header 
       (astropy.io.fits does not allow to format keyword values, or does it?)

        INPUT: 
          v (float or integer):
          fmt (string): format string

        OUTPUT:
          v (float or integer): trucated value of v according to format

        EXAMPLE:
        >>> ftsfmt(1.23456789e-15,'{:.4e}'
        >>> 1.2346e-15
        >>> fitsfmt(1.23456789e-1,'{:.4e}')
        >>> 0.1346
        >>> fitsfmt(1.23456789e3,'{:d}')
        >>> 1235
    '''

    if 'd' in fmt:
        v=int(round(v)) #nearest int 
                        #NB: int() is needed  if 
                        #    e.g. v if a numpy.float64
    else:
        v=float(fmt.format(v))
    return v

#--------------
def _myname():
#--------------
    ''' return the full name (module.name) of the calling function'''
    return os.path.splitext(os.path.basename(__file__))[0]+'.'+\
        sys._getframe(1).f_code.co_name

#------------------------
def _path_base_name(path):
#------------------------
    '''Extract file basename (contains no dot)
    '''
    file_name = os.path.basename(path)
    if '.' in file_name:
        file_name = file_name[:file_name.index('.')]
    return file_name
    
#---------------------
def _runningMean(x, N):
#---------------------
    '''Equivalent to IDL smooth(x,N)
    ''' 
    N=int(round(N))
    Nx=len(x)
    #N must be odd so that the smoothing is symetric
    if not (N % 2): 
        N+=1
    N2=N//2    
    #N/2 first and last elements are left untouched
    y = np.copy(x)
    for i in range(N2,Nx-N2):
        y[i] = np.mean(x[i-N2:i-N2+N])
    return y,N  #return also N as it may change


#--------------------------------------
def _getmin(v,nmin=None,rel_tol=1e-6):
#--------------------------------------
    '''Return location of local minima in v.
        Sorted with lowest local minimum first
    '''
    
    #get the forward and backward diff 
    #except for the last and first points respectively 
    delta=[0 if math.isclose(v[i+1],v[i],rel_tol=rel_tol) else v[i+1]-v[i] 
           for i in range(len(v)-1)]+[0]
    nabla=[0,*delta[:-1]]

    #if there is a plateau affect the rightmost non-zero value to delta 
    while True:
        flat=[k for k in range(len(v)-1) if delta[k]==0]
        if any([delta[k+1] for k in flat]):
            for k in flat: delta[k]=delta[k+1]
        else:
            break
    #if there is a plateau affect the leftmost non-zero value to nabla        
    while True:
        flat=[k for k in range(1,len(v)) if nabla[k]==0]
        if any([nabla[k-1] for k in flat]):
            for k in flat: nabla[k]=nabla[k-1]
        else:
            break
    minloc=[k for k in range(len(v)) if delta[k]>0 and nabla[k]<0] 

    #sorted with lowest local minimum first
    vm=[v[k] for k in minloc]
    minloc=[x for _,x in sorted(zip(vm,minloc))]
    if nmin == None: 
        return minloc
    else:
        return minloc[:nmin]

#------------------------------------
def _smooth_line(lbda,spectrum,DL):
#------------------------------------
    '''Return 
         -The smoothed spectrum 
           (runing mean over DL (given in same unit as lbda))
         -The wavelength value at the minimum of the smoothed spectrum
         -The effective width used  
    ''' 
    dlbda=np.mean(lbda[1:]-lbda[0:-1])
    Nwidth=np.clip(DL/dlbda,3,len(spectrum)/20)
    smooth_spectrum,Nwidth=_runningMean(spectrum,Nwidth)
    return smooth_spectrum,lbda[np.argmin(smooth_spectrum)],Nwidth*dlbda

#--------------------------------------------------------------------------
def _initcoef(data,CDELT3,lbda,spectrum,nmin=2,fres=2.e0,ax=None):
#--------------------------------------------------------------------------
    '''Return the initial parameter for the fit
    '''
    deg=3
    NAXIS3=len(data)
    
    #Smoothing the reference spectrum
    smooth_spectrum,lbda0,DL=_smooth_line(lbda,spectrum,CDELT3*fres)

    #locate observation minima
    minloc=_getmin(data,nmin)
    nmin=len(minloc) #can be less than the requested nmin

    #try polynomial fit from these minima 
    #and select the one with minimum residuals
    chi=np.zeros(nmin) #sum of square residuals
    coeff=np.zeros((nmin,deg+1)) # array of polynomial coeff 
    sampled_spectrum=np.zeros((nmin,NAXIS3))
    for i in range(nmin):
        lbda_data=(np.arange(NAXIS3)-minloc[i])*CDELT3+lbda0
        sampled_spectrum[i,:]=np.interp(lbda_data,lbda,smooth_spectrum)
        lss=sampled_spectrum[i,:]*(lbda_data-lbda0)
        lss2=lss*(lbda_data-lbda0)
        A = np.vstack([lss2,lss,sampled_spectrum[i,:],np.ones(len(lss))]).T
        pcoeff,res,rk= np.linalg.lstsq(A, data, rcond=None)[0:3]
        chi[i]=np.sum((data-np.matmul(A,pcoeff))**2)
        coeff[i,:]=pcoeff
    lcm=np.argmin(chi)
    lmin=minloc[lcm]
    
    #plot observed versus sampled smoothed reference 
    #and polynomial fit
    if ax != None:
        ss=sampled_spectrum[lcm,:]
        ags=ss.argsort()
        #xs=np.linspace(min(ss),max(ss),100)
        #ax.plot(ss,data,'r+',xs,np.polyval(coeff[lcm,:],xs),'k-')
        lbda_data=(np.arange(NAXIS3)-minloc[lcm])*CDELT3+lbda0
        lss=ss*(lbda_data-lbda0)
        lss2=lss*(lbda_data-lbda0)
        A = np.vstack([lss2,lss,ss,np.ones(len(lss))]).T
        Y=np.matmul(A,coeff[lcm,:])
        ax.plot(ss,data,'r+',ss[ags],Y[ags],'k-')
        ax.set_xlabel('Reference spectrum intensity')
        ax.set_ylabel('Observed intensity')
    
    return [lmin,fres]+list(coeff[lcm,:]),lbda0,smooth_spectrum,DL

#----------------------------------------
def fitskey2axe(NAXIS,CRPIX,CRVAL,CDELT):
#----------------------------------------
    ''' Return an array with NAXIS values from CRPIX,CRVAL and CDELT 
        FITS keywords values
    '''
    #FITS index starts at 1 (convention for CRPIX index))
    return (np.arange(1,NAXIS+1)-CRPIX)*CDELT+CRVAL

#------------------------------------------------------------------------------------
def make_specobs(data,center_npix=None,center_key=('CENTER_X','CENTER_Y'),xoffset=0):
#------------------------------------------------------------------------------------
    '''Build Observed spectrum from datacube

       Input: 
               data (string or tuple): 
                     (data,header) tuple.
                     Can be obtained from the FITS file by using:
                     data= astropy.io.fits.getdata(file,header=True)

               center_npix (int or (int,int) or None, optional): 
                     Take an average around the center of each image 
                     (box of size (2*center_npix+1)**2 or, if a tuple 
                      is provided: (2*center_npix[0]+1)*(2*center_npix[1]+1)
                     None => take the mean of the whole images 

               center_key (2 string tuple,optional): 
                     (not used if center_npix==None) 
                     FITS keywords that gives solar disk center.

               xoffset (int): box offset from disk center in x-direction
 
       Output:
               specobs (array) : observed spectrum 
    ''' 
    cube=data[0]
    header=data[1]
    if center_npix != None: #mean around center
        try:
            dx=center_npix[0]
            dy=center_npix[1]
        except:
            dx=dy=center_npix
        CX=int(round(header[center_key[0]]))-1  #-1 because FITS index starts at 1
        CY=int(round(header[center_key[1]]))-1  
        imin,imax=xoffset+CX-dx,xoffset+CX+dx
        jmin,jmax=CY-dy,CY+dy
        specobs=np.mean(cube[:,jmin:jmax+1,imin:imax+1],axis=(1,2)) 
    else: #integrated images
        specobs=np.mean(cube,axis=(1,2)) 
    return specobs

#----------------------------------------------------------
def load_specref(reffile=None,refdir='',WAVELNTH=None,
                 refurl='https://observations-solaires.obspm.fr/sites/oss/IMG/zip/',
                 exturl='.fits-2.zip'):
#---------------------------------------------------------
    '''
    Get the reference spectrum from file or from reference wavelength

       Input: (all optional)
              reffile (string or None): 
                     Full path to the FITS file of the reference 
                     spectrum. 
                      
               refdir (string): (used only if reffile==None)  
                     Directory path for reference fits files.
                     If reffile is not provided, the reference spectrum
                     file xxxx.fits[*] will be searched for in this 
                     directory (where xxxx is the nearest integer to 
                     WAVELNTH ).

               WAVELNTH (float): (used only if reffile==None)
                     The reference wavelength.

               refurl (string): URL for reference spectrum files

               exturl (string): extention of reference files on refurl 

        Output: 
                lbda_ref (array): wavelengths of the reference spectrum values
                
                specref (array): reference spectrum values
    '''
    if reffile != None: #if provided  try any extension    
        reffiles=glob.glob(reffile+'*')
    else: #if not provided deduce the reffile name from data
        assert WAVELNTH != None,'If reffile in not provided, WAVELNTH must be given.'
        reffile=str(int(round(WAVELNTH)))
        #check for reffile in refdir and refurl
        url=refurl+reffile+exturl
        reffile+='.fits'
        reffile=os.path.join(refdir,reffile)
        reffiles=glob.glob(reffile+'*')+[url]

    for rfile in reffiles:
        try:
            tab=fits.getdata(rfile)
            if _debug: print('Found reference file: '+rfile)
            break
        except:
            if _debug: print('Cannot read: '+rfile)
            pass
    else:
        raise FileNotFoundError(errno.ENOENT, os.strerror(errno.ENOENT),reffile)
           
    lbda_ref=tab[0,:]
    specref=tab[1,:]

    return lbda_ref,specref

#---------------------------------------------
def _WSSR(X,lbda_ref,specref,specobs,CDELT3,X0=None):
#---------------------------------------------
    '''Function to be maximized = -(Weigthed Sum of Square Residuals)
    '''
    if(X0 == None):
        lmin,fres=X[:2] 
        pcoef=X[2:] 
    else: #we normalize by first gues so that all elements of X 
          #have same scale
        lmin,fres=np.array(X[:2])*np.array(X0[:2]) 
        pcoef=np.array(X[2:])*np.array(X0[2:])
    
    NAXIS3=len(specobs) #Number of observed samples
    DL=CDELT3*fres #smoothing width
   
    #smoothing the reference spectrum
    specref_smooth,lbda0,DL=_smooth_line(lbda_ref,specref,DL)
   
    #Obs min location (lmin) is associated to the wavelength 
    # of the smoothed reference min (lbda0).
    #The rest of the observed wavelengths are deduced from CDELT3
    lbda_obs=(np.arange(NAXIS3)-lmin)*CDELT3+lbda0 
    
    #Samples the smoothed reference spectrum at observed wavelengths
    specref_sampled=np.interp(lbda_obs,lbda_ref,specref_smooth)
    
    #Rescales the observed spectrum to the scale 
    #of the smoothed reference spectrum
    with warnings.catch_warnings():
        warnings.simplefilter("ignore")
        #specobs_scaled=(-pcoef[1]+np.sqrt(pcoef[1]**2-4.0*(pcoef[2]-specobs)
        #                                  *pcoef[0]))/(2.0*pcoef[0])
        D,C,B,A=pcoef
        num=(B+(lbda_obs-lbda0)*(C+(lbda_obs-lbda0)*D))
        specobs_scaled=(specobs-A)/num
    
    #Computing the Weighted Sum of Squared Residuals
    #lots of weight are given to the points close to lbda0
    w=1.0/(((lbda_obs-lbda0))**4+1.0) 
    WSSR=np.nanmean((specobs_scaled-specref_sampled)**2*w)/np.nanmean(w)
    
    if _debug: print(X,WSSR) 
    return WSSR

#----------------------------------------------------------------------------
def plot_fit(specobs,ref,CRPIX3,CRVAL3,CDELT3,A,B,C,D,DL,WSSR=None,
             ax=None,title='',fmt=_fmt,yscale='obs'):
#----------------------------------------------------------------------------
    ''' Display the observed spectrum and its fit with the 
        smoothed, sampled and scaled reference spectra

        INPUT: 
           specobs (array) : observed spectrum

           ref (tuple[2]):
              lbda_ref (array): Wavelengths of the reference spectrum
              specref (array): Reference spectrum

           CRPIX3 (float): 1-index (fractional) of reference wavelength.
           CRVAL3 (float): Reference wavelength. 
           CDELT3 (float): wavelenth sampling interval in observed spect.
           A,B,C,D (float*4) : 4 coefficients of  scaling.
            specobs=A+(B+C*(lbda_ref-CRPIX3)+D*(lbda_ref-CRPIX3)^2)*specref
           DL (float): smoothing width (effective resolution)
           WSSR (float, optional): Weigthed Sum of Squares
                                      (computed if not provided)
           
           ax (AxesSubplot, optional): object from e.g. 
                                         fig,ax=plt.subplots()
                                         if None a new figure is created.

           title (string, optional) : plot title
     
           fmt (list of 8 or 9 strings): format strings for 
                                                  CRPIX3,CRVAL3....,DL,[WSSR]

           yscale (string): 'obs' plot the scaled smoothed and sample reference
                                  spectrum on top of observed one
                            'ref' plot the scaled observed sectrum on top of 
                                  the scaled smoothed reference
                            'ref_full' plot the scaled observed sectrum on top 
                                       of the fll resolution reference

        OUTPUT: fig,ax  (only if ax=None on input)
    '''
    
    n=8 if WSSR==None else 9
    assert len(fmt)>=n, 'fmt should have at least '+str(n)+' format strings'

    #if WSSR is not provided calculate it and take default format
    if WSSR == None and len(fmt)==8:
        fmt=fmt[:8]+[_fmt[8]]

    #build format dictionary for conveniance   
    dfmt=dict(zip(('CRPIX3','CRVAL3','CDELT3','A','B','C','D','DL','WSSR'),fmt[:9]))
    
    pcoef=[D,C,B,A]
    
    lbda_ref,specref=ref

    NAXIS3=len(specobs)

    #Wavelength corresponding to oberved values
    lbda_obs=fitskey2axe(NAXIS3,CRPIX3,CRVAL3,CDELT3)
    
    #smoothed reference spectrum 
    specref_smoothed,lbda0=_smooth_line(lbda_ref,specref,DL)[:2]

    if yscale == 'obs':
        #sampled=smoothed reference spectrum interpolated at obs wavelengths 
        specref_sampled=np.interp(lbda_obs,lbda_ref,specref_smoothed)
        #re-scaling sampled spectrum using the polyomial coefficients
        #specref_scaled=np.polyval(pcoef,specref_sampled)
        lss=(lbda_obs-CRVAL3)*specref_sampled
        lss2=lss*(lbda_obs-CRVAL3)
        specref_scaled=A+B*specref_sampled+C*lss+D*lss2

    if yscale[:3] == 'ref':
        #specobs_scaled=(-pcoef[1]+np.sqrt(pcoef[1]**2-4.0*(pcoef[2]-specobs)
        #                                  *pcoef[0]))/(2.0*pcoef[0])
        num=(B+(lbda_obs-CRVAL3)*(C+(lbda_obs-CRVAL3)*D))
        specobs_scaled=(specobs-A)/num
        

    newfig=False
    if ax == None: #initiate a new figure
        newfig=True
        fig,ax=plt.subplots(nrows=1,ncols=1,figsize=[6.4*1.5,4.8])
    
    #red cross= observed, full black line=sampled reference specra
    if yscale=='obs':
        ax.plot(lbda_obs,specobs,'r+',lbda_obs,specref_scaled,'k-')
    else:
        g=(lbda_ref>=min(lbda_obs))&(lbda_ref<=max(lbda_obs))
        if yscale=='ref_full':
            ax.plot(lbda_obs,specobs_scaled,'r+',lbda_ref[g],specref[g],'k-')
        else:
            ax.plot(lbda_obs,specobs_scaled,'r+',lbda_ref[g],
                    specref_smoothed[g],'k-') 
    
    #add an anoted vertical blue line at reference wavelength
    ax.axvline(x=CRVAL3,color='b',linestyle='--')
    ax.text(CRVAL3, np.mean(ax.get_ylim()),
            dfmt['CRVAL3'].format(CRVAL3).strip()+'$\mathrm{\AA}$',
            verticalalignment='center',horizontalalignment='right',
            color='b',rotation=90)

    #set axs labels and plot title
    ax.set_xlabel('Wavelength [$\mathrm{\AA}$]')
    if yscale=='obs':
        ax.set_ylabel('Observed intensity')
    else:
        ax.set_ylabel('Reference intensity')
    
    if WSSR==None:
        lmin=CRPIX3-(CRVAL3-lbda0)/CDELT3-1.
        fres=DL/CDELT3
        WSSR=_WSSR((lmin,fres,D,C,B,A),*ref,specobs,CDELT3)
    
    WSSR=fitsfmt(WSSR,dfmt['WSSR']) #truncate as in FITS file header
    title+=' - WSSR='+dfmt['WSSR'].format(WSSR)
    ax.set_title(title)
 
    #trucate as in FITS file header
    CRPIX3,CRVAL3,CDELT3,A,B,C,D,DL=map(fitsfmt,[CRPIX3,CRVAL3,CDELT3,
                                                 A,B,C,D,DL],fmt[:8])

    #insert text showing A,B,C,DL and CRPIX3 values
    #at the bottom left of the plot 
    text ='a='+dfmt['A'].format(A)+'\n'
    text+='b='+dfmt['B'].format(B)+'\n'
    text+='c='+dfmt['C'].format(C)+'\n'
    text+='d='+dfmt['D'].format(D)+'\n'                                
    text+='$\delta\lambda$='+\
        dfmt['DL'].format(DL).strip()+' $\mathrm{\AA}$'+'\n'
    text+='CRPIX3='+dfmt['CRPIX3'].format(CRPIX3)
    ax.text(0.005, 0., text, horizontalalignment='left',
            verticalalignment='bottom', transform=ax.transAxes)

    #insert CDELT3 at bottom right
    text='CDELT3='+dfmt['CDELT3'].format(CDELT3)+' $\mathrm{\AA}$'
    ax.text(1., 0., text, horizontalalignment='right',
        verticalalignment='bottom', transform=ax.transAxes)

    if(newfig): 
        return fig,ax

#-----------------------------------------------------------------------
def profilefit(data,reffile=None,refdir='',CDELT3=None,fres=2.0,nmin=2,
               center_npix=None,center_key=('CENTER_X','CENTER_Y'),xoffset=0,
               showall=False,plot=False,memo='_profilefit',figpath='',
               update=False,key=default_key):
#------------------------------------------------------------------------
    '''Calibrate the wavelength axis of Spectro-heliographe datacubes.
    
          Input: 
               data (string or tuple): 
                     Path to the cube FITS file or (cube,header) tuple.
                     In the later case, the tuple can be obtained from 
                     the file by using:
                     data= astropy.io.fits.getdata(file,header=True)

               reffile (string): 
                     Full path to the FITS file providing the reference 
                     spectrum. 
                      
               refdir (string): (used only if reffile==None)  
                     Directory path for reference fits files.
                     If reffile is not provided, the reference spectrum
                     file xxxx.fits[*] will be looked for in this 
                     directory (where xxxx is the nearest integer to 
                     the WAVELNTH keyword obtained from the data).   
           
               CDELT3 (float,optional): 
                     If provided it overwrites the CDELT3 keyword of 
                     the FITS file.
          
               fres (float,optional): 
                     Ratio between the effective resolution and CDELT3.
                     For the initial fit (before optimization), the 
                     width of the smoothing window applyed to the 
                     reference spectrum  is fres*CDELT3.

               nmin (int,optional):
                     The number of observed local minima that are tried as
                     initial location of the reference wavelength.
                     Increasing this number may help in patological cases.

               center_npix (int or (int,int) or None, optional): 
                     Take an average around the center of each image 
                     (box of size (2*center_npix+1)**2 or, if a tuple 
                      is provided: (2*center_npix[0]+1)*(2*center_npix[1]+1)
                     None => take the mean of the whole images 

               center_key (2 string tuple,optional): 
                     (not used if center_npix==None) 
                     FITS keywords that gives solar disk center.

               xoffset (int): box offset from disk center in x-direction
     
               plot (True/False or string, optional):
                     in ['',False]: no figure 
                     is ['var'] : no figure displayed 
                                  and no figure file produced
                                  but fig variable is returned (see Output: fig)
                     in ['win',True]: figure on display window only 
                     in ['png,'jpg','eps'...]: create figure file only (see Output: fig)
                     in ['win+png', 'win+jpg'... ]: window + file (see Output: fig)

               showall (boolean,optional): (not used if plot in
                                                   ['',False])   
                     If False then display only the optimized fit.
                     If True then plot also the polynomial fit and 
                     the resulting initial profile fit.
                                   
               memo (string, optional): (not used if plot in 
                                         ['',False,True,'win'])
                     String attached to cube basename to form the output 
                     file name. Example: 
                                  for plot='png' and memo='_profilefit'
                                  cube.fits.gz -> cube_profilefit.png
          
               figpath (string, optional): (not used if plot in 
                                            ['',False,True,'win']) 
                     Directory path for figure file.
                     '' =>  current directory

               update (logical, optional):
                     If data is a (cube,header) tuple and update==True 
                     then header is updated (but not the file on disk).
     ***CAUTION=====>If data is a path to the FITS file and update==True 
                     then the FITS file header is updated on disk.
                       
               key (optional):  
                      List of 9 [keyword,comment,format string] lists 
                      that will be used  for updating the header with the 
                      9 values of the output dict (see below). 
                      Format stings are also used for the figure anotations. 
                      The default values can be obtained from  
                                                     tc_shgcalib.default_key.

        Output: {key[0][0]:CRPIX3,
                 key[1][0]:CRVAL3,
                 key[2][0]:CDELT3,
                 key[3][0]:A,
                 key[4][0]:B,
                 key[5][0]:C,
                 key[6][0]:D,
                 key[7][0]:DL,
                 key[8][0]:WSSR},
                 header,
                 [fig]
            where:        
                CRPIX3  : 1-index location of CRVAL3 (cube 3rd axis)
                  
                CRVAL3  : equal to WAVELNTH cube header keyword

                CDELT3  : equal to CDELT3 cube header keyword
                           or to the one provided as input
                  
                A,B,C,D: 4 elements array of 2nd degree polynomial scaling
                        obs=A+(B+C*(lambda-lambda_c)+D*(lambda-lambda_c)**2)*(ref)        
                       (where obs is the observed spectrum and ref is
                       the smoothed reference one)  

                DL      : Effective resolution (same unit as WAVELNTH)

                             
                WSSR    : Weigthed Sum of Squared Residuals 
                         (Quality index:the lower the better)
                         If optimization step failed -WSSR is returned.

                header  : The updated  header if update==True
                          The initial  header if update==False

                [fig]   : (optional) output figure file name (full path)  
                          (only if a figure file is produced) 
                          or figure variable (if plot is 'var')
                          see also plot and memo input argument.

    NOTE: If data is a file path then the following FITS keywords are red 
          from the file: WAVELNTH, CDELT3, DATE-OBS (or DATE_OBS)
          If data is a (cube,header) tuple, the above keywords + FILENAME 
          must be keys of the header dictionary object.  

    EXAMPLES: 
    >>> from tc_shgcalib import profilefit

    1-Passes the fits file and shows the 3 plots (on screen)
    
    >>> file='spectro_obspm_fullprofile_Cak_3D_20181210_104007.fits.gz' 
    >>> reffile='3934.fits'
    >>> P,h=profilefit(file,reffile,plot=True,showall=True)
    >>> print(h['DATE_OBS'],*P.values())

    2-Reads the fits file first, passes data and prints the updated header
    (file does not change on disk, no display on screen and no figure file)

    >>> from astropy.io import fits
    >>> data = fits.getdata(file,header=True)
    >>> P,h=profilefit(data,reffile,update=True)
    >>> print(repr(h))

    3-Updates the file header and creates a jpg file of final fit 
    in directory '/tmp'
    (no display on screen, file changes on disk)

    >>> P,h,figname=profilefit(file,update=True,plot='jpg',figpath='/tmp')
   

    ''' 
    fmt=[t[2] for t in key] # extract key format strings
    kwd=[t[0] for t in key] # extract keywords

    if(plot == True):
        plot='win'

    #Read the cube and extract keywords
    try:
        dataisfile=True
        cubename=_path_base_name(data)
        if(update): #kepp the file opened so we can update the header later on
            hdul = fits.open(data,'update')
            cube,header=hdul[0].data, hdul[0].header
        else:
            cube,header= fits.getdata(data,header=True)
    except:
        dataisfile=False
        cube=data[0]
        header=data[1].copy()
        cubename=_path_base_name(header['FILENAME'])
    
    try: #prefer the standard 'DATE-OBS' keyword if it exists   
        DATEOBS=header['DATE-OBS']
    except: # try 'DATE_OBS' alternative
        DATEOBS=header['DATE_OBS']

    if CDELT3 == None: #read CDELT3 from file only if not provided as argument 
       CDELT3=header['CDELT3']

    CRVAL3=header['WAVELNTH'] 
    
    #read reference spectrum
    ref=load_specref(reffile=reffile,refdir=refdir,WAVELNTH=CRVAL3)
    lbda_ref,specref=ref

    #Build Observed spectrum from datacube
    specobs=make_specobs((cube,header),center_npix,center_key,xoffset)

    #initiate figure if needed
    if plot and showall:
        fig,ax=plt.subplots(nrows=3,ncols=1,figsize=[6.4*1.5,4.8*2])
        ax0=ax[0]
        ax0.set_title(DATEOBS)
    else:
        ax0=None
    
    #get initial parameters 
    X0,lbda0,specref_smooth,DL=_initcoef(specobs,CDELT3,lbda_ref,
                                          specref,fres=fres,nmin=nmin,ax=ax0)
    lmin,fres=X0[:2]
    pcoef=X0[2:]
    
    CRPIX3=(CRVAL3-lbda0)/CDELT3+lmin+1. #FITS starts at index 1
    
    #Get the corresponding Weighted Sum os Square residuals
    ancdata=(*ref,specobs,CDELT3)
    WSSR=_WSSR(X0,*ancdata)

    #initial parameters
    P0=(CRPIX3,CRVAL3,CDELT3)+tuple(pcoef[::-1])+(DL,WSSR)

    #plot the fit corresponding to the initial parameters
    if plot and showall:
        title='Before optimization'
        plot_fit(specobs,ref,*P0,ax=ax[1],title=title,fmt=fmt)
    
    #optimization  
    ancdata=(*ancdata,X0)
    Xi=np.ones(len(X0))                   
    res=minimize(_WSSR,Xi,args=ancdata,method='Nelder-Mead',
                 options={'maxiter':1000,'maxfev':5000,
                          'adaptive':True,'disp':_debug,
                          'xatol':1.e-2,'fatol':1.})
    #print(res.x*X0,res.fun,res.nit)
    res=minimize(_WSSR,res.x,args=ancdata,method='Nelder-Mead',
                 options={'maxiter':1000,'maxfev':5000,
                          'adaptive':True,'disp':_debug,
                          'xatol':1.e-4,'fatol':1.})
    #print(res.x*X0,res.fun,res.nit)
    if not res.success: print('Optimization failed: '+res.message)
    X=res.x*X0
    WSSR=res.fun  if res.success else -res.fun
    lmin,fres=X[:2]
    pcoef=X[2:]
    DL=CDELT3*fres
    specref_smooth,lbda0,DL=_smooth_line(lbda_ref,specref,DL)
    CRPIX3=(CRVAL3-lbda0)/CDELT3+lmin+1.

    #output tuple
    P=(CRPIX3,CRVAL3,CDELT3)+tuple(pcoef[::-1])+(DL,WSSR)
    dP = collections.OrderedDict(zip(kwd,P))

    #plot the fit after optimization
    figname=None
    if plot:
        title='With optimization'
        if showall:
            plot_fit(specobs,ref,*P,ax=ax[2],title=title,fmt=fmt)
            plt.subplots_adjust(top=0.92, bottom=0.1, left=0.11, right=0.95, 
                                hspace=0.45,wspace=0.35)
        else:
            title=DATEOBS+' '+title
            fig=plot_fit(specobs,ref,*P,title=title,fmt=fmt)[0]

        if(plot[:3].lower() == 'win'):
            fig.show()
            input('Press enter to continue...')
            ext=plot[4:].strip()
        else:
            ext=plot
        if(ext is not 'var' and ext):
            figname=os.path.join(figpath,cubename+memo+'.'+ext)
            #print('Writing file: '+figname)
            fig.savefig(figname,format=ext)
        if(ext is not 'var'):
            plt.close(fig)

    if update:
        #update/add keywords
        fP=list(map(fitsfmt,P,fmt))#format output for FITS file header
        for (kw,comment,ft),val in zip(key,fP):
            header[kw]=(val,comment)
        
        #update and close fits file on disk
        if dataisfile:
            #add history line    
            date = datetime.datetime.now().strftime("%Y-%m-%d %H:%M")
            history=date+'-Wavelength axis calibration ('+_myname()+')'
            header.add_history(history)
            print('Updating file header: '+data)
            hdul.close()
    
    if figname is None:
        if plot is 'var':
            return dP,header,fig
        else:
            return dP,header
    else:
        return dP,header,figname

if __name__=="__main__":

    if len(sys.argv)>1:
        import argparse
        parser = argparse.ArgumentParser()
        parser.add_argument("file", help="full_profile fits file")
        parser.add_argument("-u","--update", help="update fits file header", 
                            action="store_true")
        parser.add_argument("-n","--noplot", help="no plot on display",
                            action="store_true")
        args = parser.parse_args()
        plot=not args.noplot
        P,h=profilefit(args.file,plot=plot,showall=plot,update=args.update)
        print(h['DATE_OBS'])
        for (key,cmt,fmt) in default_key:
            print(key,'=',fitsfmt(P[key],fmt),'/',cmt)
    else:
        print('Running tests on 6 files...')
        import astropy.utils.data

        file=astropy.utils.data.download_file('ftp://ftpbass2000.obspm.fr/pub/meudon/spc/K/1707/spectro_obspm_fullprofile_Cak_3D_20170721_113844.fits.gz',cache=True)
        WSSR=round(profilefit(file)[0]['CAL_WSSR'])
        if WSSR > 259:
            print('Fail on Fig. 1 (259): ', WSSR)
        else:
            print('Fig. 1 (259) ok: ',WSSR)

        file=astropy.utils.data.download_file('ftp://ftpbass2000.obspm.fr/pub/meudon/spc/K/1709/spectro_obspm_fullprofile_Cak_3D_20170922_131839.fits.gz',cache=True)
        WSSR=round(profilefit(file)[0]['CAL_WSSR'])
        if WSSR > 382:
            print('Fail on Fig. 2 (382): ', WSSR)
        else:
            print('Fig. 2 (382) ok: ',WSSR)

        file=astropy.utils.data.download_file('ftp://ftpbass2000.obspm.fr/pub/meudon/spc/K/1707/spectro_obspm_fullprofile_Cak_3D_20170705_115420.fits.gz',cache=True)
        WSSR=round(profilefit(file)[0]['CAL_WSSR'])
        if WSSR > 864:
            print('Fail on Fig. 3 (864): ', WSSR)
        else:
            print('Fig. 3 (864) ok: ',WSSR)

        file=astropy.utils.data.download_file('ftp://ftpbass2000.obspm.fr/pub/meudon/spc/K/1707/spectro_obspm_fullprofile_Cak_3D_20170703_123037.fits.gz',cache=True)
        WSSR=round(profilefit(file)[0]['CAL_WSSR'])
        if WSSR > 460:
            print('Fail on Fig. 4 (460): ', WSSR)
        else:
            print('Fig. 4 (460) ok: ',WSSR)

        file=astropy.utils.data.download_file('ftp://ftpbass2000.obspm.fr/pub/meudon/spc/K/1811/spectro_obspm_fullprofile_Cak_3D_20181127_082427.fits.gz',cache=True)
        WSSR=round(profilefit(file)[0]['CAL_WSSR'])
        if WSSR > 14290:
            print('Fail on Fig. 5 (14290): ', WSSR)
        else:
            print('Fig. 5 (14290) ok: ',WSSR)

        file=astropy.utils.data.download_file('ftp://ftpbass2000.obspm.fr/pub/meudon/spc/K/1710/spectro_obspm_fullprofile_Cak_3D_20171031_081010.fits.gz',cache=True)
        WSSR=round(profilefit(file,CDELT3=0.093)[0]['CAL_WSSR'])
        if WSSR > 148993:
            print('Fail on Fig. 6 (148993): ', WSSR)
        else:
            print('Fig. 6 (148993) ok: ',WSSR)

