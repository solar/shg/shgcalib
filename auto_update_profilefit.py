#!/usr/bin/env python3
import numpy as np
import os,glob,ftplib,re,sys,astropy.time,dateutil.parser
from shgcalib.tc_shgcalib import default_key, profilefit, fitsfmt
import pandas as pd
from datetime import date
from shgcalib.plot_param import plot_param, plotly_param
#
def _filename2timestamp(file_name):
    '''
    Extract a date time index from a file name assuming 
    that it contains '_yyyymmdd_hhmm'
    '''
    match=re.search('_(\d{8})_(\d{6})',file_name)
    if match:
        return pd.to_datetime(match.group(1)+' '+match.group(2))

def _remove_processed(files,dir=None,csvfile=None):
    '''
    Filter out files that have already been processed.

    Input:
          files: file names list or generator
          dir: directory path
          csvfile: csv filename
    Output: 
          file names generator

    Method:
          If a directory path is provided, we check if a file with the same 
          basename without extension already exists in the directory. 
          If this is the case, the file is ignored.

          If a csv filename is provided, we check the first column 
          (asumed to be time indexes) and compare with the time index extracted 
          from the each file name. If a match is found the file is ignored.
    '''
    assert not(dir and csvfile), 'Please provide either dir or csvfile or none, not both'
    if csvfile:
        data = pd.read_csv(csvfile,index_col=0,parse_dates=True,sep='\t')

    for file in files:
        if dir:
            #basename without extension
            basename=os.path.basename(file).split('.')[0] 
            #Check if a file with the same basename already exists in dir
            #if not then yield the file.
            if (len(glob.glob(os.path.join(dir,basename)+'*'))==0):
                yield(file) 
        elif csvfile:
            time_stamp=_filename2timestamp(file)
            if time_stamp not in data.index:
                yield(file)
        else:
            yield(file)

def update_profilefit(files,figpath_root,csvfile,WSSR_limit=None):
    '''
    Take a list of 3D spectroheliograph FITS file names (or ftp path)
    and update profile fit figures and the csv file of the profile 
    fit parameters
    
    Input:
        files: list of 3D spectroheliograph FITS file names or ftp path

        figpath_root: root directory path for figures.
                      png files will be stored in directoy figpath_root/yymm
                      where yymm is obtained from the DATE-OBS keyword
                      of the FITS headers.
 
        csvfile: name (with path) of the csv file where the parameters 
                 resulting from the pofile fits are stored. 
    '''
    #CDELT3 header wrong between these two dates
    datenew=pd.to_datetime('2017-10-16T01:00:00') 
    dateok=pd.to_datetime('2018-07-12T01:00:00') 
    column_names=[key[0] for key in default_key]
    if os.path.exists(csvfile):
        param=pd.read_csv(csvfile,index_col=0,parse_dates=True,sep='\t')
        assert all(param.columns==column_names),f'{csvfile}: Wrong table format'
    else:
        param=pd.DataFrame(columns=column_names)

    for file in files:
        data= astropy.io.fits.getdata(file,header=True)
        dobs=data[1]['DATE_OBS']
        #Correct DATE_OBS format when needed 
        #(yyyymmddThhmmss instead of yyyy-mm-ddThh:mm:ss.xxx)
        if dobs[8] == 'T': 
            dobs=(dobs[0:4]+'-'+dobs[4:6]+'-'+dobs[6:11]+':'+dobs[11:13]+
                  ':'+dobs[13:15]+'.000')
        wavelnth=data[1]['WAVELNTH']
        dateindex=pd.to_datetime(dobs) 
        yymm=f'{dateindex.year}{dateindex.month:02}'[2:] 
        figpath=os.path.join(figpath_root,yymm) 
        if not os.path.exists(figpath):
            os.makedirs(figpath)
        CDELT3=None    
        if datenew <= dateindex < dateok:
            if wavelnth ==  3968.468 or wavelnth == 3933.663:
                CDELT3=0.093 
            elif wavelnth == 6562.762:
                CDELT3=0.155
                
        print('Processing: '+dobs,flush=True)
        P,_,figname=profilefit(data,CDELT3=CDELT3,plot='png',figpath=figpath)
        param.loc[dateindex,:]=pd.Series(P)
        print('See file: '
              +figname.replace('/mingus-DEPOT',
                               'https://solar-ftp.oca.eu/pub/DEPOT'))

    param=param.loc[~param.index.duplicated(keep='last')]
    param.sort_index(inplace=True)
    param.to_csv(csvfile,float_format='%15.7e',sep='\t')
    plot_param(csvfile,fig=True,show=False,figpath_root=figpath_root,WSSR_limit=WSSR_limit)

def auto_update_profilefit(hosturl,pathurl_root,
                           regexp_filter,figpath_root,csvfile,WSSR_limit=np.inf,yymm=None):
    '''
    Look for 3D spectroheliograph FITS files on an ftp server
    and update profile fit figures and the csv file of the profile 
    fit parameters for files that have not already been processed 
    (i.e. with an entry in the csv file)

    Input:
       hosturl: 
            ftp server name (not including the 'ftp://' prefix)

       pathurl_root: 
            path on the ftp server. FITS files for the year yy and month mm 
            are assumed to be located in the directory pathurl_root/yymm

       regexp_filter:
            Regular expression that will match with de desired FITS files

       figpath_root:
            Path to the directory where output figures are stored.
            Figures for the year yy and month mm 
            will be located in the directory figpath_root/yymm

       csvfile:
            Name of the ascci file in which all the profile fit parameters are stored.
            Each line corresponds to a different FITS file (different time index)

       yymm: 
            Gives the month and year for which we want to update profile fit figures 
            and the csv file. If yymm==None then the current year and month is taken.
            If yymm='all' then all pathurl_root subdirectories are scaned for new files.
  
    Output:
       Number of new files that have been processed.
    '''
    files=[]
    if str(yymm).strip().lower() == 'all':
        with ftplib.FTP(hosturl) as ftp:
            ftp.login()
            diryymm=ftp.nlst(pathurl_root)
            for dir in diryymm:
                files.extend(ftp.nlst(dir))
    else:
        if yymm is None:
            today = date.today()
            yymm = today.strftime("%Y%m")[2:]
        yymm=str(yymm)
        pathurl=os.path.join(pathurl_root,yymm)
        figpath=os.path.join(figpath_root,yymm)  
        with ftplib.FTP(hosturl) as ftp:
            ftp.login()
            files = ftp.nlst(pathurl)
 
    #filter out already processed files.
    filtered_files=(re.match(regexp_filter,file)[0] for file in files 
                    if re.match(regexp_filter,file))
    if os.path.exists(csvfile):
        files=_remove_processed(filtered_files,csvfile=csvfile)
    else:
        files=filtered_files
    url_file_list=['ftp://'+hosturl+file for file in files]
    file_count=len(url_file_list)
    if file_count==0 :
        if str(yymm).strip().lower() == 'all':
            print(f'No new file found in ftp://{hosturl}{pathurl_root}')
        else:
            print(f'No new file found in ftp://{hosturl}{os.path.join(pathurl_root,yymm)}')
    else:
        update_profilefit(url_file_list,figpath_root,csvfile,WSSR_limit=WSSR_limit)
    return file_count

if __name__=="__main__":
    import argparse

    Cah=dict(hosturl      = 'ftpbass2000.obspm.fr',
             pathurl_root = '/pub/meudon/spc/H/',
             regexp_filter= '.*spectro_obspm_fullprofile_Cah_3D.*\.fits\.gz$',
             figpath_root = '/mingus-DEPOT/RESULTS/SHG_CAIIH/',
             csvfile      = '/mingus-DEPOT/RESULTS/SHG_CAIIH/spectro_obspm_fullprofile_Cah_3D.csv',
             WSSR_limit   = 10_000)

    Cak=dict(hosturl      = 'ftpbass2000.obspm.fr',
             pathurl_root = '/pub/meudon/spc/K/',
             regexp_filter= '.*spectro_obspm_fullprofile_Cak_3D.*\.fits\.gz$',
             figpath_root = '/mingus-DEPOT/RESULTS/SHG_CAIIK/new2/',
             csvfile      = '/mingus-DEPOT/RESULTS/SHG_CAIIK/new2/spectro_obspm_fullprofile_Cak_3D.csv',
             WSSR_limit   = 10_000)

    ha=dict(hosturl       = 'ftpbass2000.obspm.fr',
            pathurl_root  = '/pub/meudon/spc/Ha/',
            regexp_filter = '.*spectro_obspm_fullprofile_ha_3D.*\.fits\.gz$',
            figpath_root  = '/mingus-DEPOT/RESULTS/SHG_HA/',
            csvfile       = '/mingus-DEPOT/RESULTS/SHG_HA/spectro_obspm_fullprofile_ha_3D.csv',
            WSSR_limit    = 100_000)

    param={'cah':Cah,'cak':Cak,'ha':ha}
    parser = argparse.ArgumentParser()
    parser.add_argument("line",help="data type",choices=param.keys(),nargs='*')
    parser.add_argument("--yymm", help="Year and month")
    args = parser.parse_args()
    file_count=0
    for line in args.line:
        file_count+=auto_update_profilefit(**param[line],yymm=args.yymm)
    if(file_count != 0):
        plotly_param(html=True,show=False,
                     cak_csv=Cak['csvfile'],
                     cah_csv=Cah['csvfile'],
                     ha_csv =ha['csvfile'],
                     figpath_root='/mingus-var-ftp/SHG/',
                     WSSR_limit={'ha':ha['WSSR_limit'],'cak':Cak['WSSR_limit'],'cah':Cah['WSSR_limit']})
        sys.exit(2)
